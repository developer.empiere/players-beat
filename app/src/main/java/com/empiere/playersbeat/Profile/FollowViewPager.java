package com.empiere.playersbeat.Profile;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.empiere.playersbeat.Following.Following_F;
import com.empiere.playersbeat.SimpleClasses.ApiRequest;
import com.empiere.playersbeat.SimpleClasses.Callback;
import com.empiere.playersbeat.SimpleClasses.Fragment_Callback;
import com.empiere.playersbeat.SimpleClasses.Variables;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FollowViewPager  extends FragmentPagerAdapter {
    Context context;
        public FollowViewPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0)
            {
               fragment= new Following_F(new Fragment_Callback() {
                    @Override
                    public void Responce(Bundle bundle) {

                        Call_Api_For_get_Allvideos();

                    }
                });
                Bundle args = new Bundle();
                args.putString("id", Variables.sharedPreferences.getString(Variables.u_id,""));
                args.putString("from_where","following");
                fragment.setArguments(args);

            }
            else if (position == 1)
            {
                fragment= new Following_F(new Fragment_Callback() {
                    @Override
                    public void Responce(Bundle bundle) {

                        Call_Api_For_get_Allvideos();

                    }
                });
                Bundle args = new Bundle();
                args.putString("id", Variables.sharedPreferences.getString(Variables.u_id,""));
                args.putString("from_where","fan");
                fragment.setArguments(args);
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0)
            {
                title = "Following";
            }
            else if (position == 1)
            {
                title = "Followers";
            }

            return title;
        }
    private void Call_Api_For_get_Allvideos() {

        JSONObject parameters = new JSONObject();
        try {
            parameters.put("my_fb_id", Variables.sharedPreferences.getString(Variables.u_id,""));
            parameters.put("fb_id", Variables.sharedPreferences.getString(Variables.u_id,""));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiRequest.Call_Api(context, Variables.showMyAllVideos, parameters, new Callback() {
            @Override
            public void Responce(String resp) {
                Parse_data(resp);
            }
        });



    }
    public void Parse_data(String responce){

        try {
            JSONObject jsonObject=new JSONObject(responce);
            System.out.println(jsonObject+"response");
            String code=jsonObject.optString("code");
            if(code.equals("200")) {
                JSONArray msgArray = jsonObject.getJSONArray("msg");

                JSONObject data = msgArray.getJSONObject(0);
                JSONObject user_info = data.optJSONObject("user_info");
                // username2_txt.setText(user_info.optString("username"));
                //username.setText(user_info.optString("first_name")+" "+user_info.optString("last_name"));

              /*  Profile_F.pic_url=user_info.optString("profile_pic");
                Picasso.with(context)
                        .load(Profile_F.pic_url)
                        .placeholder(context.getResources().getDrawable(R.drawable.profile_image_placeholder))
                        .resize(200,200).centerCrop().into(imageView);*/

                //  follow_count_txt.setText(data.optString("total_following"));
                //   fans_count_txt.setText(data.optString("total_fans"));
                //  heart_count_txt.setText(data.optString("total_heart"));


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    }
