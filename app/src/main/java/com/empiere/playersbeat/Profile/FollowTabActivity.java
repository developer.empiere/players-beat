package com.empiere.playersbeat.Profile;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.empiere.playersbeat.Following.Following_F;
import com.empiere.playersbeat.R;
import com.empiere.playersbeat.SimpleClasses.Fragment_Callback;
import com.empiere.playersbeat.SimpleClasses.Variables;
import com.empiere.playersbeat.SimpleClasses.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class FollowTabActivity extends AppCompatActivity {

    protected TabLayout tabLayout;
    protected ViewPager menu_pager;
    private ViewPagerAdapter adapter;
    private ImageView ivBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_tab);
        Set_tabs();
    }


    public void Set_tabs(){
        menu_pager = (ViewPager) findViewById(R.id.viewpager);
        menu_pager.setOffscreenPageLimit(2);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        FollowViewPager  viewPagerAdapter = new FollowViewPager(getSupportFragmentManager());
        menu_pager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(menu_pager);
        ivBack = findViewById(R.id.ivBack);
       /* ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/

    }

}
